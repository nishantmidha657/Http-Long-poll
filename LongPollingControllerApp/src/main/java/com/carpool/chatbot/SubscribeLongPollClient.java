/*package com.carpool.chatbot;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import javax.annotation.PostConstruct;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


public class SubscribeLongPollClient {

	@Value("server.longpoll.address")
	public String longPollServerUrl;

	@Value("server.chatbot.address")
	public String chatbotServerUrl;

	public String botQueryResposne;

	@PostConstruct
	void initialize() throws IOException {
		subscribeLongPoll();
	}

	public void subscribeLongPoll() throws IOException {

		while (true) {
			HttpURLConnection httpURLConnection = sendHttpRequest("GET",
					botQueryResposne, longPollServerUrl);

			botQueryResposne = null;

			// Recievd query for chatbot backend
			String botQuery = IOUtils.toString(httpURLConnection
					.getInputStream());

			// Send query to chatbot backend
			HttpURLConnection httpURLConnection1 = sendHttpRequest("POST",
					botQuery, chatbotServerUrl);
			// Set the bot response which will go as new subscribe body
			this.botQueryResposne = IOUtils.toString(httpURLConnection1
					.getInputStream());

		}

	}

	private HttpURLConnection sendHttpRequest(String typeOfRequest,
			String body, String serverUrl) throws IOException {

		URL url = new URL(serverUrl);
		URLConnection con = url.openConnection();

		HttpURLConnection httpURLConnection = (HttpURLConnection) con;
		httpURLConnection.setRequestMethod(typeOfRequest);
		if (body != null) {
			httpURLConnection.getOutputStream().write(body.getBytes());
		}
		httpURLConnection.connect();

		return httpURLConnection;
	}

}
*/