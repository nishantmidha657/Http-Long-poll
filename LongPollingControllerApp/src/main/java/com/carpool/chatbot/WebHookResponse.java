package com.carpool.chatbot;

public class WebHookResponse {

	String webHookResponse;

	public String getWebHookResponse() {
		return webHookResponse;
	}

	public void setWebHookResponse(String webHookResponse) {
		this.webHookResponse = webHookResponse;
	}
}
