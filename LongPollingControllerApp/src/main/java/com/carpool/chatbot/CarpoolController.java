package com.carpool.chatbot;

import java.io.IOException;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

@RestController
public class CarpoolController {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(CarpoolController.class);

	Queue<String> queue = new ArrayBlockingQueue<>(10);

	WebHookResponse webHookResponse = new WebHookResponse();

	@RequestMapping(value = "/request", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
	public String handleWebhook(@RequestBody String webhookRequest,
			HttpServletRequest request, HttpServletResponse response1) {
		LOGGER.info("Received webhook request as {} {} ", request,
				webhookRequest);

		queue.add(webhookRequest);

		synchronized (queue) {

			if (queue.size() > 0) {
				LOGGER.debug("Notifying for a new webhook request");
				queue.notifyAll();

			}
		}

		synchronized (webHookResponse) {
			LOGGER.trace("Inside synchronized block of Webhookresponse {}",
					webHookResponse.getWebHookResponse());

			if (webHookResponse.getWebHookResponse() == null) {
				LOGGER.debug("Wating for response");

				try {
					webHookResponse.wait();
				} catch (Exception exception) {
					LOGGER.info("Exception occured in handle Webhhok {}",
							exception.getMessage());
					LOGGER.error("Exception occured in handle Webhhok {}",
							exception);
				}
				LOGGER.info("Response for webhook query {}",
						webHookResponse.getWebHookResponse());
				String resposne = webHookResponse.getWebHookResponse();
				webHookResponse.setWebHookResponse(null);
				return resposne;
			}
		}

		LOGGER.info("Request processed successfully");

		return null;
	}

	@RequestMapping(value = "/subscribeLongPoll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public void handleLongPollingRequest(
			@RequestBody(required = false) String subscriptionBody,
			HttpServletRequest httpServletRequest, HttpServletResponse response) {

		LOGGER.info("Received subscription request as {}", subscriptionBody);

		if (subscriptionBody != null) {
			synchronized (webHookResponse) {
				LOGGER.info("Send response for bot query");
				webHookResponse.setWebHookResponse(subscriptionBody);
				webHookResponse.notifyAll();
			}
		}
		synchronized (queue) {

			if (queue.isEmpty()) {
				LOGGER.info("Waiting for bot request");
				try {
					queue.wait();
				} catch (Exception exception) {
					LOGGER.info("Exception in handleLongPollingRequest {}",
							exception.getMessage());
					LOGGER.error("Exception in handleLongPollingRequest {}",
							exception);
				}

			}
			LOGGER.info("Got a new webhhok query");

			String request = queue.poll();

			if (request != null) {

				LOGGER.debug("Found request body {}", request);
				try {
					response.setContentType(MediaType.APPLICATION_JSON_VALUE);
					response.getWriter().print(request);
				} catch (Exception exception) {
					LOGGER.info("Exception in response part ");
				}
			}
		}

		LOGGER.info("Sent response");

		// return null;
	}

	@ExceptionHandler
	private void handleException(Exception ex) {
		LOGGER.debug("Received exception : ", ex);
	}
}
