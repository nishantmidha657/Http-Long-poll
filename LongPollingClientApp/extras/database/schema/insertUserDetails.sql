\c carpool_db;

delete from user_detail;

insert into user_detail values('nishant.midha', 'Nishant', 'Midha', now());
insert into user_detail values('deepanshu.gupta', 'Deepanshu', 'Gupta', now());
insert into user_detail values('girish.kumar', 'Girish', 'Kumar', now());
insert into user_detail values('richa.nagpal', 'Richa', 'Nagpal', now());
insert into user_detail values('ashish.jain', 'Ashish', 'Jain', now());
insert into user_detail values('pragya.singh', 'Pragya', 'Singh', now());
insert into user_detail values('praveen.srivastava', 'Praveen', 'Srivastava', now());
insert into user_detail values('shubham.agarwal' , 'Shubham', 'Agarwal', now());
insert into user_detail values('shakshi.gulati', 'Shakshi', 'Gulati', now());
insert into user_detail values('shweta.gulati', 'Shweta', 'Gulati', now());
