package com.carpool.chatbot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class App {
	
	private static ApplicationContext context;
	
    public static void main(String[] args) {
        context = SpringApplication.run(App.class, args);
        //displayAllBeans();
    }
    
    private static void displayAllBeans() {
    	String[] beans = context.getBeanDefinitionNames();
    	for(String bean: beans) {
    		System.out.println("Spring bean = " + bean);
    	}
    }
}