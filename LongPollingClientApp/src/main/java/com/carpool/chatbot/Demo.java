package com.carpool.chatbot;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import org.apache.commons.io.IOUtils;

public class Demo {
	static HttpURLConnection connection;

	public String botQueryResponse;

	public static void main(String[] args) throws IOException {

		System.out.println("Sending request:main");
		/*
		 * URL url=new URL("http://127.0.0.1:8080/subscribeLongPoll");
		 * URLConnection con = url.openConnection(); HttpURLConnection
		 * httpURLConnection=(HttpURLConnection)con;
		 * 
		 * httpURLConnection.setRequestMethod("GET");
		 * httpURLConnection.connect();
		 * 
		 * connection = httpURLConnection; //con.connect();
		 * System.out.println("ssssss"+IOUtils.toString(con.getInputStream()));
		 */

		Demo demo = new Demo();
		demo.subscribeLongPoll();
		System.out.println("Response received:main");
	}

	public void subscribeLongPoll() throws IOException {
		int count = 0;
		while (true) {
			System.out.println("Sending request");
			HttpURLConnection httpURLConnection = sendHttpRequest("GET",
					botQueryResponse, "http://127.0.0.1:8080/subscribeLongPoll");

			this.botQueryResponse = null;

			// Recievd query for chatbot backend
			String botQuery = IOUtils.toString(httpURLConnection
					.getInputStream());
			System.out.println(botQuery);
			/*
			 * // Send query to chatbot backend HttpURLConnection
			 * httpURLConnection1 = sendHttpRequest("POST", botQuery,
			 * "http://127.0.0.1:8081/");
			 */
			// Set the bot response which will go as new subscribe body
			/*
			 * this.botQueryResponse = IOUtils.toString(httpURLConnection1
			 * .getInputStream());
			 */

			this.botQueryResponse = "Nishant Midha";
			System.out.println(this.botQueryResponse);

		}

	}

	private HttpURLConnection sendHttpRequest(String typeOfRequest,
			String body, String serverUrl) throws IOException {

		URL url = new URL(serverUrl);
		URLConnection con = url.openConnection();

		HttpURLConnection httpURLConnection = (HttpURLConnection) con;
		httpURLConnection.setRequestMethod(typeOfRequest);
		if (body != null) {
			httpURLConnection.setDoOutput(true);
			httpURLConnection.getOutputStream().write(body.getBytes());
		}

		httpURLConnection.connect();
		return httpURLConnection;
	}

}
